#include <iostream>
#include "bitmap/bitmap_image.hpp"
#include <stack>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <set>
// Compilar con comando -> g++ -std=c++11 fichero.cpp

using namespace std;

vector<vector<struct node>> dfs_non_recursive(int x,int y, vector<vector<struct node>> matrix,int totX,int totY);
void draw_maze( vector<vector<struct node>> matrix,int resX,int resY);
vector<vector<struct node>> eler(int x,int y,vector<vector<struct node>> matrix,int totX, int totY);
void print_stack(stack<int> c);
void print_set(set<int> c);


int cont;
vector<int> proxim;
vector<int> curr;

// Porcentaje de algoritmo de eller 
int horizontal = 50;
int vertical = 50; // higher = more union
int maxIter = 1000; // numero de iteraciones para generar rvertical

// Estructura "nodo" donde guardaremos informazion sobre cada punto del grafo
struct node{
    /// left bottom right top
    vector<bool> wall = {true,true,true,true};
    int x=-1,y=-1;
    int li = 4;
    bool visited;
    bool starting=false;
    bool end =false;
};

// generador de numeros reales aleatorios entre 0-1
double ri(){
    
    double i = ((double)rand())/(RAND_MAX);
    return i;
}

// generador de numeros enteros aleatorios entre 0-a
int rr(int a){
    return (int) (ri()*(double)a);
}



void randomJoin(){
        for(int i=1; i<curr.size()-1; i+=2){ 
            if(curr[i] == -1 && curr[i-1] != curr[i+1] && rr(100)>horizontal){
                curr[i] = 0; //destruye el muro
                int replace  = max(curr[i-1],curr[i+1]);
                int new_set = min(curr[i-1],curr[i+1]);
                for(int j=0; j<curr.size(); j++){ // Combina los dos conjuntos al de menor valor
                    if(curr[j] == replace){
                        curr[j] = new_set;
                    }
                }
            }
        }
}
void rvertical_join(){
        int inicio;   
        int fin;          
        bool bandera;  
        int iter;
        int i;
        inicio = 0;
        //busca el fin del set
        do{
            i=inicio;
            while(i<curr.size()-1 && curr[i] == curr[i+2])i+=2;
            // con inicio,fin de set
            fin = i;
            bandera = false;
            for(int p=0; p<=maxIter && !bandera;p++){
            for(int j=inicio; j<=fin; j+=2){
                if(rr(100)>vertical || p==maxIter){
                    proxim[j] = curr[j];
                    bandera = true;
                    if(p==maxIter){break;}
                }
            }
            }

            inicio = fin+2;  
            // mientras no procese el ultimo set, recorre los nodos 

        }while(fin != curr.size()-1);
}

vector<vector<struct node>> last(int row,vector<vector<struct node>> matrix){
       for(int i=0; i<curr.size(); i++){
                    curr[i] = proxim[i];
                    proxim[i] = -2;
                }
        // Une todos los sets distintos
        // Creando un grafo completamente conexo
        for(int i=1; i<curr.size()-1; i+=2){
            if(curr[i] == -1 && curr[i-1] != curr[i+1]){
                curr[i] = 0;
                int old  = max(curr[i-1],curr[i+1]);
                int proximt = min(curr[i-1],curr[i+1]);

                for(int j=0; j<curr.size(); j++){
                    if(curr[j] == old)
                        curr[j] = proximt;
                }
            }
        }

        for(int k=0; k<curr.size(); k++){

                if(curr[k] != -1 && k%2==1){
                    matrix[k/2][matrix[0].size()-1].wall[2] = false;
                    matrix[k/2+1][matrix[0].size()-1].wall[0] = false;
                }
        }
        return matrix;
}

vector<vector<struct node>> eller(int row,int col,vector<vector<struct node>> matrix){
        int act_rows = row;
        int act_cols = col;
    
        int rows = act_rows*2+1;
        int cols = act_cols*2+1;
   
    curr.resize(col*2-1);
    proxim.assign(col*2-1,-2);
   
     for(int i=0; i<curr.size(); i+=2){
            curr[i] = i/2+1;
            if(i != curr.size()-1)
                curr[i+1] = -1;
        }
    cont = curr[curr.size()-1];


      for(int q=0; q<act_rows-1; q++){   

            if(q != 0){

                //asignar la nueva fila
                for(int i=0; i<curr.size(); i++){
                    curr[i] = proxim[i];
                    proxim[i] = -2;
                }
            }
            //Conexiones horizontales y verticales
           randomJoin(); 
            rvertical_join();
            
            // Rellenar de numeros las casillas sin asignacion de set
            for(int j=0; j<curr.size(); j+=2){

                if(proxim[j] == -2)
                    proxim[j] = ++cont;

                if(j != curr.size()-1)
                    proxim[j+1] = -1;
            }
            
            // Asignar a la matriz de adyacencia
               for(int k=0; k<curr.size(); k++){
                
                if(curr[k] != -1 && k%2==1){
                    matrix[k/2][q].wall[2] = false;
                    matrix[k/2+1][q].wall[0] = false;
                }
                if(curr[k]==proxim[k] &&k%2==0){
                    matrix[k/2][q].wall[3] = false;
                    matrix[k/2][q+1].wall[1] = false;
                }

            }

        }

       matrix = last(rows,matrix);
       
    return matrix;
}


 // Metodo principal
 int main (){
     srand(time(NULL));
     // resolucion X,Y de la imagen resultado.
     // Max tried 30.000 x 30.000, ~ 3GB   if u want to see anything stay under 10k in resX or resY
    int resX = 1000;
    int resY = 1000;
     // Tamaño en filas / columnas del laberinto
    int totX = 25;   // Numero de filas
    int totY = 25;   // Numero de columnas

    
    cout << "Introduzca resolucion x y de la imagen:\n";
    cin >> resX >> resY;
    cout << "Introduzca ancho y alto del laberinto:" << endl;
    cin >> totX >> totY;

    // matrix de nodos, que actua como "matriz de adyacencia"
     vector<vector<struct node>> matrix(totY,vector<struct node>(totX));
     
     // Punto de salida ( generado al azar )
    int y = rr(totX);
    int x = rr(totY);
    int yy = rr(totX);
    int xx = rr(totY);
   
   

   
   
    matrix = eller(totX,totY,matrix);
    matrix[x][y].starting=true;
    matrix[xx][yy].end=true;
    draw_maze(matrix,resX,resY);
   
   return 0;
     
 }






 void draw_maze( vector<vector<struct node>> matrix,int resX,int resY){
     
     int sizeX = resX/matrix.size();
     int sizeY = resY/matrix[0].size();

     bitmap_image image(resX,resY);
     image.set_all_channels(0, 0, 0);
     image_drawer draw(image);

    draw.pen_width(2);
    draw.pen_color(0, 142, 17);
    for(int i=0 ; i<matrix.size(); i++){
        for(int jj= matrix[i].size()-1; jj>=0; jj-- ){
            int tj = (matrix[i].size()-1)-jj;
            int ti = (i*sizeX);
            tj =( tj*sizeY );

            if(matrix[i][jj].starting){
                
                draw.pen_width(4);
                draw.pen_color(255, 0, 0);
                for(int n=sizeX/2; n>3;n--){
                draw.rectangle(ti+n,tj+n,ti+sizeX-n,tj+sizeY-n);
                }
                draw.pen_width(2);
                draw.pen_color(0, 142, 17);
            }
             if(matrix[i][jj].end){
                
                draw.pen_width(4);
                draw.pen_color(0, 0, 255);
                for(int n=sizeX/2; n>3;n--){
                draw.rectangle(ti+n,tj+n,ti+sizeX-n,tj+sizeY-n);
                }
                draw.pen_width(2);
                draw.pen_color(0, 142, 17);
            }

            if(matrix[i][jj].wall[0]==true){
                draw.line_segment(ti,tj,ti,tj+sizeY);
            }
            if(matrix[i][jj].wall[1]==true){
                draw.line_segment(ti,tj+sizeY,ti+sizeX,tj+sizeY);
            }
            if(matrix[i][jj].wall[2]==true){
                draw.line_segment(ti+sizeX,tj+sizeY,ti+sizeX,tj);
            }
            if(matrix[i][jj].wall[3]==true){
                draw.line_segment(ti+sizeX,tj,ti,tj);
            }
        }
    }
     
     

   image.save_image("maze.bmp");

 }


 